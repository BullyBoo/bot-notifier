package entities

data class Chat(
    val id: Long,
    var notificationCounter: Int = 0,
    val notificationsList: ArrayList<Notification> = arrayListOf()
) {

    data class Notification(
        var id: Int,
        var name: String,
        var description: String? = null,
        var hour: Int,
        var minute: Int,
        var members: Set<String> = setOf()
    )
}
