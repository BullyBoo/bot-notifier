package utils

import java.io.File

object FileUtils {

    fun readFile(fileName: String): String {
        val file = File(fileName)

        return if(!file.exists()){
            file.createNewFile()
            ""
        } else {
            File(fileName).useLines {
                it.joinToString(
                    separator = ""
                )
            }
        }
    }

    fun writeToFile(fileName: String, text: String) {
        File(fileName).writeText(text)
    }
}