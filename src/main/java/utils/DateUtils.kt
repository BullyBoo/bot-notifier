package utils

import extensions.isPast
import extensions.isWeekend
import java.util.*

object DateUtils {

    private const val DAY: Long = 1000 * 60 * 60 * 24

    fun getMillisDelay(hour: Int, minutes: Int): Long {
        val calendar = Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY, hour)
            set(Calendar.MINUTE, minutes)
            set(Calendar.SECOND, 0)
        }

        while (calendar.isWeekend() || calendar.isPast()){
            calendar.timeInMillis = calendar.timeInMillis + DAY
        }

        val delay = calendar.timeInMillis - System.currentTimeMillis()

        return if(delay <= 0){
            calendar.timeInMillis += DAY

            while (calendar.isWeekend()){
                calendar.timeInMillis += DAY
            }

            calendar.timeInMillis
        } else {
            delay
        }
    }
}