package controller

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.UpdatesListener
import commands.BaseCommand
import commands.MemberCommands
import commands.NotificationCommands
import extensions.sendMessageMarkdown
import managers.CacheManager
import managers.scheduler.SchedulerManager
import utils.DateConverter
import utils.DateMask
import java.util.*
import kotlin.collections.ArrayList

object Main {

    private const val BOT_TOKEN = "1731052622:AAFovOHLXAQRbeZBwMzDAiLTcBPqdvqZtlw"

    private val chatCommandsList: ArrayList<ChatCommands> = arrayListOf()

    private var bot = TelegramBot(BOT_TOKEN)

    @JvmStatic
    fun main(args: Array<String>) {
        val cacheManager = CacheManager()
        val schedulerManager = SchedulerManager(cacheManager) { chatId, text ->
            val time = DateConverter.parse(Calendar.getInstance(), DateMask.HH_MM)

            println("Try to send Notification $time")

            bot.sendMessageMarkdown(chatId, text)

            println("Message sent!")
        }

        cacheManager.getChats().forEach { chat ->
            chat.notificationsList.forEach {
                schedulerManager.schedule(chat, it.id)
            }
        }

        startBotListener(cacheManager, schedulerManager)
    }

    private fun startBotListener(
        cacheManager: CacheManager,
        schedulerManager: SchedulerManager
    ) {
        bot.setUpdatesListener({ it ->
            it.forEach { update ->

                update.message()?.let { message ->

                    val chatId = message.chat().id()
                    val chatCommands = getChatCommands(chatId, bot, cacheManager, schedulerManager)

                    chatCommands.commands.forEach commands@{
                        val result = it.execute(message)

                        if (result) {
                            return@commands
                        }
                    }
                }
            }

            UpdatesListener.CONFIRMED_UPDATES_ALL
        }, {
            println(it)
            println(it.message)

            it.printStackTrace()

            bot.removeGetUpdatesListener()
//            bot.shutdown()

            bot = TelegramBot(BOT_TOKEN)
            startBotListener(cacheManager, schedulerManager)
        })
    }

    private fun getChatCommands(
        chatId: Long,
        bot: TelegramBot,
        cacheManager: CacheManager,
        schedulerManager: SchedulerManager
    ): ChatCommands {
        var chatCommands = chatCommandsList.firstOrNull { it.chatId == chatId }

        if(chatCommands == null){
            chatCommands = ChatCommands(
                chatId = chatId,
                commands = listOf(
                    NotificationCommands(
                        bot,
                        cacheManager,
                        schedulerManager
                    ),
                    MemberCommands(
                        bot,
                        cacheManager
                    )
                )
            )

            chatCommandsList.add(chatCommands)
        }

        return chatCommands
    }

    data class ChatCommands(
        val chatId: Long,
        val commands: List<BaseCommand>
    )
}