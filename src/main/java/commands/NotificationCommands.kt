package commands

import managers.CacheManager
import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.Message
import constants.Texts.BOT_NAME
import constants.Texts.NOTIFICATION_REMOVE_DATA
import constants.Texts.NOTIFICATION_ADD_DATA
import constants.Texts.NOTIFICATION_ADD_ERROR
import constants.Texts.NOTIFICATION_EMPTY
import constants.Texts.NOTIFICATION_ERROR
import constants.Texts.NOTIFICATION_ADD_SUCCESS
import constants.Texts.NOTIFICATION_CHANGE_DATA
import constants.Texts.NOTIFICATION_CHANGE_SUCCESS
import constants.Texts.SUCCESS_NOTIFICATION_REMOVE
import entities.Chat
import extensions.getMessageFullText
import extensions.getMessageShortText
import extensions.sendMessage
import extensions.sendMessageMarkdown
import managers.scheduler.SchedulerManager

class NotificationCommands(
    bot: TelegramBot,
    private val cacheManager: CacheManager,
    private val schedulerManager: SchedulerManager
) : BaseCommand(
    bot
) {

    companion object {
        private const val ADD_NOTIFICATION = "/add_notification"
        private const val GET_NOTIFICATION = "/get_notifications"
        private const val REMOVE_NOTIFICATION = "/remove_notification"
        private const val CHANGE_NOTIFICATION = "/change_notification"
        private const val ADD_CANCEL = "/cancel_add_notification"
        private const val REMOVE_CANCEL = "/cancel_remove_notification"
        private const val CHANGE_CANCEL = "/cancel_change_notification"

        private const val KEY_NOTIFICATION_ID = "notificationId"
        private const val KEY_NAME = "name"
        private const val KEY_DESCRIPTION = "description"
        private const val KEY_TIME = "time"
    }

    private var isStartToAddNotification = false
    private var isStartToRemoveNotification = false
    private var isStartToChangeNotification = false

    override fun execute(message: Message): Boolean {
        val chatId = message.chat().id()

        return when (val text = message.text().orEmpty()) {
            ADD_NOTIFICATION,
            "$ADD_NOTIFICATION$BOT_NAME" -> {
                isStartToAddNotification = true
                bot.sendMessageMarkdown(chatId, NOTIFICATION_ADD_DATA)
                true
            }
            CHANGE_NOTIFICATION,
            "$CHANGE_NOTIFICATION$BOT_NAME" -> {
                isStartToAddNotification = true

                val notifications = cacheManager.getChatNotifications(chatId)

                if(notifications.isNullOrEmpty()){
                    bot.sendMessage(chatId, NOTIFICATION_EMPTY)
                } else {
                    isStartToRemoveNotification = true

                    val messageText = NOTIFICATION_CHANGE_DATA + notifications.getMessageShortText()
                    bot.sendMessageMarkdown(chatId, messageText)
                }
                true
            }
            REMOVE_NOTIFICATION,
            "$REMOVE_NOTIFICATION$BOT_NAME" -> {
                val notifications = cacheManager.getChatNotifications(chatId)

                if(notifications.isNullOrEmpty()){
                    bot.sendMessage(chatId, NOTIFICATION_EMPTY)
                } else {
                    isStartToRemoveNotification = true

                    val messageText = NOTIFICATION_REMOVE_DATA + notifications.getMessageShortText()
                    bot.sendMessageMarkdown(chatId, messageText)
                }
                true
            }
            GET_NOTIFICATION,
            "$GET_NOTIFICATION$BOT_NAME" -> {
                val notifications = cacheManager.getChatNotifications(chatId)

                if (notifications.isNullOrEmpty()) {
                    bot.sendMessage(chatId, NOTIFICATION_EMPTY)
                } else {
                    bot.sendMessageMarkdown(chatId, notifications.getMessageFullText())
                }
                true
            }
            ADD_CANCEL,
            "$ADD_CANCEL$BOT_NAME" -> {
                isStartToAddNotification = false
                true
            }
            REMOVE_CANCEL,
            "$REMOVE_CANCEL$BOT_NAME" -> {
                isStartToRemoveNotification = false
                true
            }
            CHANGE_CANCEL,
            "$CHANGE_CANCEL$BOT_NAME" -> {
                isStartToChangeNotification = false
                true
            }
            else -> {
                when {
                    isStartToAddNotification -> {
                        parseNotificationData(text, chatId)
                        true
                    }
                    isStartToRemoveNotification -> {
                        parseNotificationsId(text, chatId)
                        true
                    }
                    isStartToChangeNotification -> {
                        changeNotification(text, chatId)
                        true
                    }
                    else -> {
                        false
                    }
                }
            }
        }
    }

    private fun parseNotificationData(text: String, chatId: Long) {
        try {
            var name = ""
            var description: String? = null
            var hour = 0
            var minute = 0

            text.split("\n").forEach {
                val split = it.split(" - ")
                val key = split[0]
                val value = split[1]

                when(key){
                    KEY_NAME -> name = value
                    KEY_DESCRIPTION -> description = value
                    KEY_TIME -> {
                        val time = value.split(":")
                        hour = time[0].toInt()
                        minute = time[1].toInt()
                    }
                }
            }

            if (hour !in 0..23 || minute !in 0..59 || name.isEmpty()) {
                bot.sendMessage(chatId, NOTIFICATION_ADD_ERROR)
            } else {
                val chat = cacheManager.getChatOrCreate(chatId)
                chat.notificationCounter++

                val notificationId = chat.notificationCounter

                cacheManager.addNotification(
                    chatId = chatId,
                    notification = Chat.Notification(
                        id = notificationId,
                        name = name,
                        description = description,
                        hour = hour,
                        minute = minute
                    )
                )
                bot.sendMessage(chatId, NOTIFICATION_ADD_SUCCESS)
                isStartToAddNotification = false

                schedulerManager.schedule(chat, notificationId)
            }
        } catch (e: Exception) {
            bot.sendMessage(chatId, NOTIFICATION_ADD_ERROR)
        }
    }

    private fun parseNotificationsId(text: String, chatId: Long) {
        try {
            val existsNotifications = text.split(" - ")[1]
                .split(" ")
                .map { it.toInt() }
                .filter { cacheManager.getNotification(chatId, it) != null }

            existsNotifications.forEach {
                cacheManager.removeNotification(chatId, it)
            }

            bot.sendMessage(chatId, String.format(SUCCESS_NOTIFICATION_REMOVE, existsNotifications.size))
            isStartToRemoveNotification = false

        } catch (e: Exception) {
            bot.sendMessage(chatId, NOTIFICATION_ERROR)
        }
    }

    private fun changeNotification(text: String, chatId: Long) {
        try {
            var notificationId = 0
            var name: String? = null
            var description: String? = null
            var hour: Int? = null
            var minute: Int? = null

            text.split("\n").forEach {
                val split = it.split(" - ")
                val key = split[0]
                val value = split[1]

                when(key){
                    KEY_NOTIFICATION_ID -> notificationId = value.toInt()
                    KEY_NAME -> name = value
                    KEY_DESCRIPTION -> description = value
                    KEY_TIME -> {
                        val time = value.split(":")
                        hour = time[0].toInt()
                        minute = time[1].toInt()
                    }
                }
            }

            val notification = cacheManager.getNotification(chatId, notificationId)

            if(notification == null){
                bot.sendMessage(chatId, NOTIFICATION_ERROR)
                return
            }

            name?.let { notification.name = it }
            description?.let { notification.description = it }
            hour?.let { notification.hour = it }
            minute?.let { notification.minute = it }


            cacheManager.changeNotification(chatId, notification)
            bot.sendMessage(chatId, NOTIFICATION_CHANGE_SUCCESS)

        } catch (e: Exception) {
            bot.sendMessage(chatId, NOTIFICATION_ADD_ERROR)
        }
    }
}