package commands

import managers.CacheManager
import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.Message
import constants.Texts.BOT_NAME
import constants.Texts.MEMBER_ADD_DATA
import constants.Texts.MEMBER_ADD_ERROR
import constants.Texts.NOTIFICATION_EMPTY
import constants.Texts.MEMBER_ADD_SUCCESS
import constants.Texts.MEMBER_GET_DATA
import constants.Texts.MEMBER_GET_EMPTY
import constants.Texts.MEMBER_GET_SUCCESS
import constants.Texts.MEMBER_REMOVE_DATA
import constants.Texts.MEMBER_REMOVE_SUCCESS
import constants.Texts.NOTIFICATION_ERROR
import extensions.getMessageShortText
import extensions.sendMessage
import extensions.sendMessageMarkdown

class MemberCommands(
    bot: TelegramBot,
    private val cacheManager: CacheManager
): BaseCommand(
    bot
) {

    companion object {

        private const val ADD_MEMBER = "/add_members"
        private const val ADD_CANCEL = "/cancel_add_members"

        private const val REMOVE_MEMBER = "/remove_members"
        private const val REMOVE_CANCEL = "/cancel_remove_members"

        private const val GET_MEMBER = "/get_members"
        private const val GET_CANCEL = "/cancel_get_members"

        private const val KEY_NOTIFICATION_ID = "notificationId"
        private const val KEY_MEMBERS = "members"
    }

    private var isStartToAddMembers = false
    private var isStartToRemoveMembers = false
    private var isStartToGetMembers = false

    override fun execute(message: Message): Boolean {
        val chatId = message.chat().id()

        return when(val text = message.text().orEmpty()){
            ADD_MEMBER,
            "$ADD_MEMBER$BOT_NAME" -> {
                val notifications = cacheManager.getChatNotifications(chatId)

                if(notifications.isNullOrEmpty()){
                    bot.sendMessage(chatId, NOTIFICATION_EMPTY)
                } else {
                    isStartToAddMembers = true

                    val messageText = MEMBER_ADD_DATA + notifications.getMessageShortText()
                    bot.sendMessageMarkdown(chatId, messageText)
                }
                true
            }
            REMOVE_MEMBER,
            "$REMOVE_MEMBER$BOT_NAME" -> {
                val notifications = cacheManager.getChatNotifications(chatId)

                if(notifications.isNullOrEmpty()){
                    bot.sendMessage(chatId, NOTIFICATION_EMPTY)
                } else {
                    isStartToRemoveMembers = true

                    val messageText = MEMBER_REMOVE_DATA + notifications.getMessageShortText()
                    bot.sendMessageMarkdown(chatId, messageText)
                }
                true
            }
            GET_MEMBER,
            "$GET_MEMBER$BOT_NAME" -> {
                val notifications = cacheManager.getChatNotifications(chatId)

                if(notifications.isNullOrEmpty()){
                    bot.sendMessage(chatId, NOTIFICATION_EMPTY)
                } else {
                    isStartToGetMembers = true

                    val messageText = MEMBER_GET_DATA + notifications.getMessageShortText()
                    bot.sendMessageMarkdown(chatId, messageText)
                }
                true
            }
            ADD_CANCEL,
            "$ADD_CANCEL$BOT_NAME"-> {
                isStartToAddMembers = false
                true
            }
            REMOVE_CANCEL,
            "$REMOVE_CANCEL$BOT_NAME"-> {
                isStartToRemoveMembers = false
                true
            }
            GET_CANCEL,
            "$GET_CANCEL$BOT_NAME"-> {
                isStartToGetMembers = false
                true
            }
            else -> {
                when {
                    isStartToAddMembers -> {
                        parseData(text, chatId){ notificationId, list ->
                            val addedMembers = cacheManager.getMembers(chatId, notificationId)

                            val members = list.filter { name -> addedMembers.none { it ==  name } }
                                .filter { isMemberNameValid(it) }

                            cacheManager.addMembers(chatId, notificationId, members)
                            bot.sendMessage(chatId, String.format(MEMBER_ADD_SUCCESS, members.size))
                            isStartToAddMembers = false
                        }
                        true
                    }
                    isStartToRemoveMembers -> {
                        parseData(text, chatId){ notificationId, list ->
                            val addedMembers = cacheManager.getMembers(chatId, notificationId)

                            val members = list.filter { name -> addedMembers.any { it ==  name } }
                                .filter { isMemberNameValid(it) }

                            cacheManager.removeMembers(chatId, notificationId, members)
                            bot.sendMessage(chatId, String.format(MEMBER_REMOVE_SUCCESS, members.size))
                            isStartToRemoveMembers = false
                        }
                        true
                    }
                    isStartToGetMembers -> {
                        parseNotificationId(text, chatId)
                        true
                    }
                    else -> {
                        false
                    }
                }
            }
        }
    }

    private fun parseData(
        text: String,
        chatId: Long,
        actionSuccess: (Int, List<String>) -> Unit
    ){
        var notificationId = 0
        var members: List<String> = arrayListOf()

        try {
            text.split("\n").forEach {
                val split = it.split(" - ")
                val key = split[0]
                val value = split[1]

                when(key){
                    KEY_NOTIFICATION_ID -> notificationId = value.toInt()
                    KEY_MEMBERS -> {
                        val list = value.split(" ").toList()

                        if(list.isEmpty()){
                            bot.sendMessage(chatId, MEMBER_ADD_ERROR)
                        } else {
                            members = list
                        }
                    }
                }
            }

            actionSuccess.invoke(notificationId, members)

        } catch (e: Exception) {
            bot.sendMessage(chatId, MEMBER_ADD_ERROR)
        }
    }

    private fun parseNotificationId(
        text: String,
        chatId: Long
    ) {

        try {
            val notificationId = text.split(" - ")[1].toInt()

            val members = cacheManager.getMembers(chatId, notificationId)

            if(members.isEmpty()){
                bot.sendMessage(chatId, MEMBER_GET_EMPTY)
            } else {
                val message = MEMBER_GET_SUCCESS + members.joinToString(separator = " ")
                bot.sendMessage(chatId, message)
            }

            isStartToGetMembers = false
        } catch (e: Exception) {
            bot.sendMessage(chatId, NOTIFICATION_ERROR)
        }
    }

    private fun isMemberNameValid(name: String): Boolean {
        return name.startsWith("@") &&
            name.length == name.replace(Regex("\\s"), "").length
    }
}