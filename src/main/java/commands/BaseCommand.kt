package commands

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.Message

abstract class BaseCommand(
    protected val bot: TelegramBot
) {

    abstract fun execute(message: Message): Boolean
}