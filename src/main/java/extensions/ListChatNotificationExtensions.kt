package extensions

import entities.Chat

fun List<Chat.Notification>.getMessageShortText(): String {
    return joinToString("\n\n") {
        "*${it.name}*\n" +
            "Идентификатор: ${it.id}\n" +
            "Время уведомления: ${it.getTime()}"
    }
}

fun List<Chat.Notification>.getMessageFullText(): String {
    return joinToString("\n\n") {
        if(it.members.isNullOrEmpty()){
            "*${it.name}*\n" +
                "Идентификатор: ${it.id}\n" +
                "Время уведомления: ${it.getTime()}"
        } else {
            "*${it.name}*\n" +
                "Идентификатор: ${it.id}\n" +
                "Время уведомления: ${it.getTime()}\n" +
                "Пользователи: " + it.members.joinToString(separator = " ")
        }
    }
}