package extensions

import entities.Chat

fun Chat.Notification.getTime(): String {

    val hourString = if(hour < 10){
        "0$hour"
    } else {
        hour.toString()
    }

    val minuteString = if(minute < 10){
        "0$minute"
    } else {
        minute.toString()
    }

    return "$hourString:$minuteString"
}