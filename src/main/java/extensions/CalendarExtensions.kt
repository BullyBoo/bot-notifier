package extensions

import java.util.*

fun Calendar.isPast(): Boolean =
    timeInMillis < System.currentTimeMillis()

fun Calendar.isWeekend(): Boolean {
    val dayOfWeek = get(Calendar.DAY_OF_WEEK)

    return dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY
}