package extensions

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.request.ParseMode
import com.pengrad.telegrambot.request.SendMessage

fun TelegramBot.sendMessage(chatId: Long, text: String){
    execute(SendMessage(chatId, text))
}

fun TelegramBot.sendMessageMarkdown(chatId: Long, text: String){
    execute(
        SendMessage(chatId, text)
            .parseMode(ParseMode.Markdown)
    )
}