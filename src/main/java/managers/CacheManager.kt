package managers

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import entities.Chat
import utils.FileUtils


class CacheManager {

    companion object {

        private const val FILE_NAME = "cache.json"
    }

    private val gson = Gson()

    private val chats: ArrayList<Chat> = arrayListOf()

    init {
        val token = object: TypeToken<List<Chat>>(){}.type

        try {
            val list = gson.fromJson<List<Chat>>(
                FileUtils.readFile(FILE_NAME),
                token
            )

            list?.let { chats.addAll(it) }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @Synchronized
    fun addNotification(chatId: Long, notification: Chat.Notification){
        val chat = getChatOrCreate(chatId)
        chat.notificationsList.add(notification)

        val source = gson.toJson(chats)
        FileUtils.writeToFile(FILE_NAME, source)
    }

    @Synchronized
    fun removeNotification(chatId: Long, notificationId: Int){
        val chat = getChatOrCreate(chatId)

        val notification = chat.notificationsList.first { it.id == notificationId }
        chat.notificationsList.remove(notification)

        val source = gson.toJson(chats)
        FileUtils.writeToFile(FILE_NAME, source)
    }

    @Synchronized
    fun changeNotification(chatId: Long, newNotification: Chat.Notification){
        val chat = getChatOrCreate(chatId)

        val notification = chat.notificationsList.first { it.id == newNotification.id }
        chat.notificationsList.remove(notification)
        chat.notificationsList.add(newNotification)

        val source = gson.toJson(chats)
        FileUtils.writeToFile(FILE_NAME, source)
    }

    @Synchronized
    fun addMembers(chatId: Long, notificationId: Int, members: List<String>){
        val notifications = getChatNotifications(chatId).orEmpty()

        val notification = notifications.first { it.id == notificationId }

        notification.members = notification.members.plus(members)

        val source = gson.toJson(chats)
        FileUtils.writeToFile(FILE_NAME, source)
    }

    @Synchronized
    fun removeMembers(chatId: Long, notificationId: Int, members: List<String>){
        val notifications = getChatNotifications(chatId).orEmpty()

        val notification = notifications.first { it.id == notificationId }

        notification.members = notification.members.minus(members)

        val source = gson.toJson(chats)
        FileUtils.writeToFile(FILE_NAME, source)
    }

    fun getChats(): List<Chat> = chats

    fun getChatOrCreate(chatId: Long): Chat {
        var chat = chats.firstOrNull{ it.id == chatId }

        if(chat == null){
            chat = Chat(chatId)
            chats.add(chat)
        }

        return chat
    }

    fun getChatNotifications(chatId: Long): List<Chat.Notification>? {
        return chats.firstOrNull { it.id == chatId }?.notificationsList
    }

    fun getNotification(chatId: Long, notificationId: Int): Chat.Notification? {
        return getChatOrCreate(chatId).notificationsList
            .firstOrNull { it.id == notificationId }
    }

    fun getMembers(chatId: Long, notificationId: Int): List<String>{
        return getChatOrCreate(chatId).notificationsList
            .firstOrNull { it.id == notificationId }
            ?.members
            ?.toList()
            .orEmpty()
    }
}