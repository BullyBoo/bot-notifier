package managers.scheduler

import entities.Chat
import managers.CacheManager

class SchedulerRunnable(
    private val cacheManager: CacheManager,
    private val chat: Chat,
    private val notificationId: Int,
    private val actionSchedule: (Chat, Int) -> Unit,
    private val actionSendMessage: (Long, String) -> Unit
): Runnable {

    override fun run() {
        val chatId = chat.id
        val notification = cacheManager.getNotification(chatId, notificationId)

        notification?.let { it ->
            val members = notification.members.joinToString(separator = " "){
                it.replace("_", "\\_")
            }

            val message = if (it.description != null){
                "*${it.name}*" +
                    "\n\n" +
                    it.description +
                    "\n\n" +
                    members
            } else {
                "*${it.name}*" +
                    "\n\n" +
                    members
            }
            actionSendMessage.invoke(chatId, message)
            actionSchedule(chat, notificationId)
        }
    }
}