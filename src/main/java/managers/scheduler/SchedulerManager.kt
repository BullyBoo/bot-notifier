package managers.scheduler

import entities.Chat
import managers.CacheManager
import utils.DateConverter
import utils.DateMask
import utils.DateUtils
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class SchedulerManager(
    private val cacheManager: CacheManager,
    private val actionSendMessage: (Long, String) -> Unit
){

    private val executor = Executors.newScheduledThreadPool(128)

    fun schedule(chat: Chat, notificationId: Int){
        val chatId = chat.id

        val runnable = SchedulerRunnable(
            cacheManager = cacheManager,
            chat = chat,
            notificationId = notificationId,
            actionSchedule = { c, n ->
                schedule(c, n)
            },
            actionSendMessage = { i, t ->
                actionSendMessage.invoke(i, t)
            }
        )

        cacheManager.getNotification(chatId, notificationId)?.let {
            val delay = DateUtils.getMillisDelay(it.hour, it.minute)
            executor.schedule(runnable, delay, TimeUnit.MILLISECONDS)

            val calendar = Calendar.getInstance().apply {
                timeInMillis += delay
            }
            val date = DateConverter.parse(calendar, DateMask.YYYY_MM_DD_T_HH_MM_SS)
            println("Notification scheduled to: $date")
        }
    }
}