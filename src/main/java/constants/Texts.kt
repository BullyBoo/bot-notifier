package constants

object Texts {

    const val BOT_NAME = "@DatsTeamNotifierBot"

    // Notification ADD
    const val NOTIFICATION_ADD_DATA = "Введите данные уведомления в формате:\n" +
        "\n" +
        "name - Название уведомления\n" +
        "description - Описание уведомления\n" +
        "time - 13:00\n" +
        "\n" +
        "Для отмены введите /cancel\\_add\\_notification."

    const val NOTIFICATION_ADD_SUCCESS = "Уведомление успешно добавлено."

    const val NOTIFICATION_ADD_ERROR = "Неправильный формат данных.\n" +
        "\n" +
        "name - Название уведомления\n" +
        "description - Описание уведомления\n" +
        "time - 13:00\n" +
        "\n" +
        "Для отмены введите /cancel\\_add\\_notification."
    // *******************************************************************************************************

    // Notification CHANGE
    const val NOTIFICATION_CHANGE_DATA = "Введите данные уведомления в формате:\n" +
        "\n" +
        "notificationId - 1\n" +
        "name - Новое название уведомления\n" +
        "description - Новое описание уведомления\n" +
        "time - 13:00\n" +
        "\n" +
        "Для отмены введите /cancel\\_change\\_notification."

    const val NOTIFICATION_CHANGE_SUCCESS = "Уведомление успешно изменено."
    // *******************************************************************************************************

    // Notification REMOVE
    const val NOTIFICATION_REMOVE_DATA = "Введите индентификаторы уведомлений.\n" +
        "Вы можете ввести несколько идентификаторов через пробел:\n" +
        "\n" +
        "notificationIds - 1 2 3\n" +
        "\n" +
        "Для отмены введите /cancel\\_remove\\_notification.\n" +
        "\n" +
        "Уведомления чата:\n"

    const val SUCCESS_NOTIFICATION_REMOVE = "Удалено уведомлений: %d"
    // *******************************************************************************************************

    // Notification General
    const val NOTIFICATION_EMPTY = "В данном чате пока не добавлены уведомления.\n" +
        "Чтобы добавить уведомление введите /add\\_notification."

    const val NOTIFICATION_ERROR = "По данному идентификатору уведомление не найдено.\n"
    // *******************************************************************************************************

    // Member ADD
    const val MEMBER_ADD_DATA = "Введите данные в формате:\n" +
        "\n" +
        "notificationId - Идентификатор уведомления\n" +
        "members - @User1 @User2 @User3\n" +
        "\n" +
        "Для отмены введите /cancel\\_add\\_members.\n" +
        "\n" +
        "Уведомления чата:\n"

    const val MEMBER_ADD_SUCCESS = "Успешно добавлено пользователей: %d."
    // *******************************************************************************************************

    // Member REMOVE
    const val MEMBER_REMOVE_DATA = "Введите данные в формате:\n" +
        "\n" +
        "notificationId - Идентификатор уведомления\n" +
        "members - @User1 @User2 @User3\n" +
        "\n" +
        "Для отмены введите /cancel\\_remove\\_members.\n" +
        "\n" +
        "Уведомления чата:\n"

    const val MEMBER_REMOVE_SUCCESS = "Успешно удалено пользователей: %d."
    // *******************************************************************************************************

    // Member GET
    const val MEMBER_GET_DATA = "Введите данные в формате:\n" +
        "\n" +
        "notificationId - Идентификатор уведомления\n" +
        "\n" +
        "Для отмены введите /cancel\\_get\\_members.\n" +
        "\n" +
        "Уведомления чата:\n"

    const val MEMBER_GET_EMPTY = "У данного уведомления нет пользователей."

    const val MEMBER_GET_SUCCESS = "Пользователи:\n"
    // *******************************************************************************************************

    // Member General
    const val MEMBER_ADD_ERROR = "Не корректный формат данных."
    // *******************************************************************************************************
}